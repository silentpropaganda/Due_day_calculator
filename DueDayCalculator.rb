require 'active_support/time'
module DueDayCalculator	

	DSTART = 9 
	DEND = 17
	WDAY = 8.hour
	def self.due_date_calc(req_time,hleft)
		if req_time.hour >= DSTART && req_time.hour < DEND && req_time.wday < 6
			hleft = hleft.hour
			due_date = req_time
			if hleft > req_time.change(hour: DEND) - req_time
				hleft -= req_time.change(hour: DEND) - req_time
				for i in 0..hleft/WDAY
					due_date += 1.day
					if due_date.wday > 5
						due_date += 2.day
					end
				end
				due_date = due_date.change(hour: 9) + (hleft % WDAY)
			else
				due_date += hleft
			end
			return due_date
		else
			return false
		end

	end
end
puts DueDayCalculator.due_date_calc(Time.new(2015,02,21,14,25),4) 
puts DueDayCalculator.due_date_calc(Time.new(2015,02,17,2,25),20)
puts DueDayCalculator.due_date_calc(Time.new(2015,02,17,10,25),6)
puts DueDayCalculator.due_date_calc(Time.new(2015,02,17,9,25),24)
puts DueDayCalculator.due_date_calc(Time.new(2015,02,20,9,25),26)
